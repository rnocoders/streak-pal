# Welcome to Streak Pal!

Hi! I'm your first ever progress tracking application, and I'm concerned about your fitness and work activities every day. I won't give you any progress reports, unlike other applications; instead, I give you streaks, which are more fun to track, and you can challenge your streaks among your friends.
# What is a Streak?
Streak is a count of how many consecutive days you have been doing a task/activity without taking a cheat day.
eg:  Gym or physical activity which is a continuous thing you need to follow in order to achieve your goal which will indirectly helps you to focus more on your work life. 

# The Importance of Work-Life Balance!
When jobs take over too much of our lives, our physical and mental health suffer. Our personal relationships suffer because we don’t have the time needed to maintain those important connections.

![Stats Screenshot](/docs/stats.png)

Coming up with a steady fitness routine can be a great way to balance your work and life. But it’s not always easy. It can be really hard to continue a regular exercise routine, especially when your priorities have been elsewhere for so long.
Three important steps to follow the routine:-

**Create a schedule** 

**Set goals**

**Stay motivated**

You will create a schedule and goals, but who will motivate you to follow them regularly and without fail? How many motivational videos are you going to watch? Don't worry; I'm Streak Pal, and I'm here to motivate you and your entire group of friends.

Organizations that prioritize the health and wellness of their employees not only have happier and more productive employees, but they are often able to recruit better-fitting and more ambitious employees.
The majority of the organization offers physical wellness centres but does not track their progress.  

So here, I, Streak Pal, will take care of the progress in a more interactive way. It's a win-win situation for both the employees and the organization.

# Steak Pal Way!
I will track your personal streaks to keep you motivated, but the cool thing about me is that you can form a group with your friends and challenge them. Everyone can see your progress if you want to let everybody know that you are on track and motivate your pals.

## How to use the Application :

The Application features a Simple User Account Setup, where user can Register himself an account an later use those details to Login every time.
**So the First Step is to Register or Login.**

![Login Register Form Screenshot](/docs/Login-Register.png)

As soon as you Login/Sign Up, a timer starts to Run. (Assuming you have started to keep the Streak) And when you break the Streak, you need to Reset, by clicking the Reset Button. This Restarts the Streak. (timer)

![Streak Screenshot](/docs/Streak.png)

**Group :**
The next feature is Group,  which allows you form a group among your friends/colleagues with the same motive. You can use the Buttons shown in above screenshot for the group features. 
- **Create Group** is to Create your own group, where you'll be the admin and you can invite your friends. 
- **Join Group** is to Join with an existing group

**View Leaderboard :**

When you're in a group, you can see the Leaderboard and find who is maintaining the pace. We believe this Ranking system helps to Motivate People in Staying with the Streak. 

**Delete Group/Leave from Group :**

When you decided to not participate in the Group, any time you can Leave from a Group or Delete the Group that you have created previously. 

## Technicals :
**Stacks:**
- Front End : Angular
- Backend : Spring Boot
- Database : PostgreSQL

We used a VM Instance to Deploy the Spring Boot and Angular App and configured the Firewall to access the Ports of VM Instance through External IP.

In the Backend, we have a simple Table Structure to Record the Data, a simple ER Diagram explains it.

![ER Diagram Screenshot](/docs/ER-Diagram.png)

**To Deploy the Following steps :**
First we Created a VM Instance and Configured the Ports 8080 and 4200 to access through External IP.

**Spring Boot :** 
- Installed JDK and PostgreSQL
- Cloned the Git Repository
- Used `mvn package` command to Build the `.war` file and using the command `nohup java -jar streak-pal-0.0.1-SNAPSHOT.war &` the Backend was made to run in `8080`

**Angular : **
- Installed Node JS
- Cloned the Git Repository
- Installed the Node Modules using `npm start` command.
- Then using the command `ng serve --host 0.0.0.0` the Frontend was made to Run in `4200`