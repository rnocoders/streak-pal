import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Group} from './group';
import {environment} from "../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class GroupService {

  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  createGroup(group: Group): Observable<any> {
    return this.httpClient.post(this.baseUrl + "/createGroup", group,{responseType: "text"});
  }

  isInGroup(userName: string): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}/isInGroup?userName=${userName}`);
  }

  listOfGroups(): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}/listOfGroups`);
  }

  joinGroup(group: Group): Observable<any> {
    return this.httpClient.put(this.baseUrl + "/joinGroup", group, {responseType: 'text'});
  }

  leaveGroup(userName: Group): Observable<any> {
    return this.httpClient.put(this.baseUrl + "/removeFromGroup", userName,{responseType:'text'});
  }

  removeCount(userName: Group): Observable<any> {
    return this.httpClient.put(this.baseUrl+ "/startCount", userName);
  }
  getLeaderBoard(groupName: string): Observable<any>{
      return this.httpClient.get(this.baseUrl+ `/viewBoard?groupname=${groupName}`);
    }

  deleteGroup(group: Group):Observable<any> {
    return this.httpClient.post(this.baseUrl + '/deleteGroup', group,{responseType: "text"});
  }
}
