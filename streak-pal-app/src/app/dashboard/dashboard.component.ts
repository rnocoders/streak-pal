import {Component, OnInit} from '@angular/core';
import {Group} from '../group';
import {GroupService} from '../group.service';
import {Router} from "@angular/router";
import {NotifierService} from "../notifier.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public userName: string | undefined;
  isInGroup: Boolean = false;
  listOfGroups: Group[] = [];
  message: any;
  error: any;
  groupName:any;
  leaderBoard:any;
  streakDate: any;
  currentDate: any;
  day: any
  hours: any;
  min: any;
  seconds: any;
  absoulteTime:any;
  group: Group = new Group();
  createGroupMessage!: string;

  constructor(private groupService: GroupService, private route: Router, private alertNotification: NotifierService) {
  }

  ngOnInit(): void {
    this.userName = localStorage.getItem('userName') ?? 'Name';
    this.group.userName = this.userName;
    this.userGroupCheck();
    this.groupList();
    this.streakCountDays();
    this.getLeaderBoard();
  }

  public userGroupCheck() {
    this.userName = localStorage.getItem('userName') ?? 'Name';
    this.groupService.isInGroup(this.userName).subscribe(data => {
      this.isInGroup = data;
      console.log("Is in a group : " + this.isInGroup);
    });
  }

 public getLeaderBoard(){
     this.groupName = localStorage.getItem('userGroupName')
     this.groupService.getLeaderBoard(this.groupName).subscribe(data => {
      console.log("data:: ",data)
      this.leaderBoard = data;
     })
   }

  public groupList() {
    this.groupService.listOfGroups().subscribe(data => {
      this.listOfGroups = data;
    });
  }

  whenCreateGroupButtonClicked() {
    this.group.userName = localStorage.getItem('userName') ?? 'Name';
    document.getElementById("createGroupModalCloseButton")?.click();
  }

  whenJoinGroupButtonClicked() {
    this.group.userName = localStorage.getItem('userName') ?? 'Name';
    document.getElementById("joinGroupModalCloseButton")?.click();
  }

  public createGroup() {
    this.group.userName = localStorage.getItem('userName') ?? 'Name';
    this.groupService.createGroup(this.group).subscribe(data => {
      this.message = data;
      this.userGroupCheck();
      console.log("Message : " + this.message)
      if(this.message === 'Group Already Exist'){
                this.alertNotification.showNotification(this.message,'ok','error','right','top');
          }else if(this.message === 'Group has been added'){
                this.alertNotification.showNotification(this.message,'ok','success','right','top');
                localStorage.setItem('userGroupName', this.group.groupName);
                this.getLeaderBoard();
          }else{
                this.alertNotification.showNotification(this.message,'ok','error','right','top');
              }

    });
    document.getElementById("createGroupModalCloseButton")?.click();
  }

  joinGroup() {
    this.groupService.joinGroup(this.group).subscribe(data => {
      this.message = data;
      console.log("Message : " + this.message)
      this.userGroupCheck();
      if(this.message === 'No Group has been found'){
                    this.alertNotification.showNotification(this.message,'ok','error','right','top');
              }else{
              localStorage.setItem('userGroupName', this.group.groupName);
              this.getLeaderBoard();
                    this.alertNotification.showNotification(this.message,'ok','success','right','top');
              }
    });

  }

  whenLeaveGroupButtonClicked() {
    this.userName = localStorage.getItem('userName') ?? 'Name';
    this.group.userName = this.userName;
    this.groupService.leaveGroup(this.group).subscribe(data => {
      this.message = data.toString();
      console.log("Message : " + this.message)
      this.userGroupCheck();
      if(this.message === 'Group Removed Successfully'){
                this.alertNotification.showNotification(this.message,'ok','success','right','top');
              }else{
                this.alertNotification.showNotification(this.message,'ok','error','right','top');
              }

    });
    document.getElementById("leaveGroupModalCloseButton")?.click();
  }

  public logout() {
    localStorage.removeItem('userName');
    localStorage.removeItem('userDisplayName');
    localStorage.removeItem('userGroupName');
    this.route.navigate(['/user-login']);
  }

  refreshStreakCount() {
    this.userName = localStorage.getItem('userName') ?? 'Name';
    this.group.userName = this.userName;
    this.groupService.removeCount(this.group).subscribe(res => {
      localStorage.setItem('userStreakDate',res.returnObject);
      this.message = res.status;
      this.streakCountDays();
      this.alertNotification.showNotification("Your Streak has been refreshed", 'OK', 'success', 'center', 'bottom');
    }, error => {
      this.error = error.returnObject;
      this.alertNotification.showNotification(this.error, 'OK', 'error', 'right', 'bottom');
    })
  }

  streakCountDays() {
    this.streakDate = localStorage.getItem('userStreakDate');
    this.streakDate= new Date(this.streakDate);
    this.currentDate = new Date();

       this.absoulteTime = Math.abs(this.currentDate - this.streakDate);

       this.day =  Math.floor(this.absoulteTime / (1000 * 60 * 60 * 24));
       this.hours = Math.floor((this.absoulteTime / (1000*60*60)) % 24 );
       this.min = Math.floor((this.absoulteTime)/(1000*60) %60);
       this.seconds = Math.floor((this.absoulteTime)/(1000) %60);


    console.log("hours in floor ",(this.absoulteTime / (1000*60*60)) % 24 );

  }


  deleteGroup() {
    this.userName = localStorage.getItem('userName') ?? 'Name';
    this.group.groupName = localStorage.getItem('userGroupName')?? "null";
    this.groupService.deleteGroup(this.group).subscribe(res =>{
     this.message = res;
     localStorage.removeItem('userGroupName');
     this.isInGroup = false;
      this.alertNotification.showNotification(this.message, 'OK', 'success', 'right', 'top');
      })
//     },error => {
//       this.error = error.error;
//       this.alertNotification.showNotification(this.error, 'OK', 'error', 'right', 'bottom');
//     })
    document.getElementById("deleteGroupModalCloseButton")?.click();
  }
}
