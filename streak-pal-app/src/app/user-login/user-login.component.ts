import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {User} from "../user";
import {RegisterService} from "../register.service";
import {NotifierService} from "../notifier.service";

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  user: User = new User();
  error:any = null;


  constructor(private httpclient: HttpClient, private route: Router, private registerService: RegisterService, private alertNotification :NotifierService) {
  }

  ngOnInit(): void {
  }

  public login() {
    this.registerService.loginService(this.user).subscribe(res => {
      let data = res;
      console.log(data);
      if(data.status == 'OK'){
        let user = data.returnObject;
        localStorage.setItem('userName', user.userName);
         if(user.group != null){
           localStorage.setItem('userGroupName', user.group.groupName);
         }

        localStorage.setItem('userStreakDate', user.userTracker.date);
      }

      this.alertNotification.showNotification('Login Successfull','ok','success','right','top');
      this.route.navigate(['/dashboard']);
    },error => {
      this.error = error.error.returnObject;
      this.alertNotification.showNotification(this.error,'Dismiss','error','center','top');
    })
  }



}
