import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { User } from '../user';
import {NotifierService} from "../notifier.service";

@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})
export class UserSignupComponent implements OnInit {

  user:User = new User();
  error = null;
  constructor(
    private registerService:RegisterService,
    private router:Router,
    private  notifierService : NotifierService
    ) { }

  ngOnInit(): void {
  }

  userRegister(){
    this.registerService.registerUser(this.user).subscribe(data=>{
      console.log(data);
      this.notifierService.showNotification("Signed Up Successfully",'OK','success','right','top');
      this.router.navigate(['/user-login']);
    },error=>{
        console.log(error.error);
        this.error = error.error;
        this.notifierService.showNotification(this.error,'OK','error','center','top');
    });
  }

}
