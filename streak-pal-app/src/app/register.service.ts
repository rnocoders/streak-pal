import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from './user';
import {environment} from "../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  registerUser(user: User): Observable<Object> {
    return this.httpClient.post(this.baseUrl+"/saveUserDetails", user, {responseType: "text"});
  }

  loginService(user: User): Observable<any> {
    return this.httpClient.post<any>(this.baseUrl+"/login", user);
  }
}
