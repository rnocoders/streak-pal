import { Injectable } from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";
import {NotifierComponent} from "./notifier/notifier.component";

@Injectable({
  providedIn: 'root'
})
export class NotifierService {

  constructor(private snackbar: MatSnackBar) { }

  showNotification(displayMsg:any,actions:any,displayColor:'success' | 'error',hPosition:'center'|'left'|'right',vPosition:'bottom'|'top'){
    this.snackbar.openFromComponent(NotifierComponent,{
      data:{
        msg:displayMsg,
        action:actions
      },
      duration:2000,
      horizontalPosition:hPosition,
      verticalPosition:vPosition,
      panelClass:displayColor
    });
  }
}
