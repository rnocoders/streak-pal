package com.streakapp.streakpal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreakPalApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreakPalApplication.class, args);
	}

}
