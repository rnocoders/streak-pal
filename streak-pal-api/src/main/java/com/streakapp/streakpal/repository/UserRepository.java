package com.streakapp.streakpal.repository;

import com.streakapp.streakpal.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    public User findByUserName(String userName);

    @Modifying
    @Transactional
    @Query("update User u set u.group.groupId =?1,u.adminFlag=?2 where u.userId =?3")
    public void updateGroupDetails(int groupId,int flag,int userId);

    @Query("select u from User u where u.group.groupId=?1")
    public List<User> viewBoardList(int groupId);


}
