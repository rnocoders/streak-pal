package com.streakapp.streakpal.repository;

import com.streakapp.streakpal.entity.UserTracker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserTrackerRepository extends JpaRepository<UserTracker, Integer> {

    @Modifying
    @Transactional
    @Query("update UserTracker ut set ut.date = ?1 where ut.userTrackerId =?2")
    public void updateDate(String date,int userTrackerId);
}
