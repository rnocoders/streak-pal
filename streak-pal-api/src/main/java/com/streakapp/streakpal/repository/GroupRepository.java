package com.streakapp.streakpal.repository;

import com.streakapp.streakpal.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group,Integer> {

    public Group findByGroupName(String groupName);


}
