package com.streakapp.streakpal.serviceImpl;

import com.streakapp.streakpal.entity.Group;
import com.streakapp.streakpal.entity.User;
import com.streakapp.streakpal.repository.GroupRepository;
import com.streakapp.streakpal.repository.UserRepository;
import com.streakapp.streakpal.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<String> createGroup(@RequestBody HashMap<String,String> input) {
        String groupName = input.get("groupName");
        String userName = input.get("userName");
        String groupGoal = input.get("groupGoal");
        String groupDescription = input.get("groupDescription");
        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(userName));
        Optional<Group> group = Optional.ofNullable(groupRepository.findByGroupName(groupName));
        if(ObjectUtils.isEmpty(user.get().getGroup())){
            if(!group.isPresent()) {
                Group createGroup = new Group();
                createGroup.setGroupName(groupName);
                createGroup.setGroupGoal(groupGoal);
                createGroup.setGroupDescription(groupDescription);
                user.get().setGroup(createGroup);
                user.get().setAdminFlag(1);
                groupRepository.save(createGroup);
                userRepository.save(user.get());
                return ResponseEntity.ok("Group has been added");
            }
            else{
                return ResponseEntity.status(HttpStatus.OK).body("Group Already Exist");
            }
        }
        else{
            return ResponseEntity.status(HttpStatus.OK).body("You are already in a  "+user.get().getGroup().getGroupName()+" group");
        }
    }

    @Override
    public ResponseEntity<String> addGroupMembers(HashMap<String,String> input) {

        String userName = input.get("userName");
        String groupName = input.get("groupName");
        Optional<User> userDetails = Optional.ofNullable(userRepository.findByUserName(userName));
        if(userDetails.isPresent()) {
            if (ObjectUtils.isEmpty(userDetails.get().getGroup())) {
                Optional<Group> groupDetails = Optional.ofNullable(groupRepository.findByGroupName(groupName));
                String groupNameString;
                if (groupDetails.isPresent()) {
                    userRepository.updateGroupDetails(groupDetails.get().getGroupId(),0, userDetails.get().getUserId());
                    groupNameString = groupDetails.get().getGroupName();
                } else {
                    return ResponseEntity.ok("No Group has been found");
                }
                return ResponseEntity.ok("You have been added to " + groupNameString);
            }
        }
        else {
            return ResponseEntity.ok("User Not Found");
        }
        return ResponseEntity.ok("You are already in a "+userDetails.get().getGroup().getGroupName()+" group");
    }

    @Override
    public ResponseEntity<String> deleteGroup(HashMap<String,String> input){
        String name = input.get("userName");
        User userDetails = userRepository.findByUserName(name);
        if(userDetails.getAdminFlag().equals(1)){
            String groupName = input.get("groupName");
            Optional<Group> group = Optional.ofNullable(groupRepository.findByGroupName(groupName));
            if(group.isPresent()){
                List<User> groupMembers = userRepository.viewBoardList(group.get().getGroupId());
                groupMembers.forEach(x->{
                    x.setGroup(null);
                    x.setAdminFlag(null);
                    userRepository.save(x);
                });
                groupRepository.delete(group.get());
                return ResponseEntity.ok("Group Deleted Successfully");
            }
            else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No Group Found");
            }
        }
        else{
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("You don't have admin privilege to delete a group");
        }
    }

    @Override
    public boolean isInGroup(String userName) {
        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(userName));
        if(ObjectUtils.isEmpty(user.get().getGroup())) {
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public List<Group> listOfGroups() {
        return groupRepository.findAll();
    }
}
