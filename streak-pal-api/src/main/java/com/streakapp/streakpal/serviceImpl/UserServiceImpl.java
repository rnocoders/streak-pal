package com.streakapp.streakpal.serviceImpl;

import com.streakapp.streakpal.dto.ExpectedReturnDTO;
import com.streakapp.streakpal.dto.UserBoardDTO;
import com.streakapp.streakpal.dto.UserDTO;
import com.streakapp.streakpal.entity.Group;
import com.streakapp.streakpal.entity.User;
import com.streakapp.streakpal.entity.UserTracker;
import com.streakapp.streakpal.repository.GroupRepository;
import com.streakapp.streakpal.repository.UserRepository;
import com.streakapp.streakpal.repository.UserTrackerRepository;
import com.streakapp.streakpal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTrackerRepository userTrackerRepository;
    @Autowired
    private GroupRepository groupRepository;

    private static String dateFinder(String start_date, String end_date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = sdf.parse(start_date);
            d2 = sdf.parse(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference_In_Time
                = d2.getTime() - d1.getTime();

        long difference_In_Seconds
                = (difference_In_Time
                / 1000)
                % 60;

        long difference_In_Minutes
                = (difference_In_Time
                / (1000 * 60))
                % 60;

        long difference_In_Hours
                = (difference_In_Time
                / (1000 * 60 * 60))
                % 24;

        long difference_In_Days
                = (difference_In_Time
                / (1000 * 60 * 60 * 24))
                % 365;

        return difference_In_Days
                + " days, "
                + difference_In_Hours
                + " hours, "
                + difference_In_Minutes
                + " minutes, "
                + difference_In_Seconds
                + " seconds";

    }

    @Override
    public ResponseEntity<String> saveUser(HashMap<String, String> input) {
        String userName = input.get("userName");
        String password = input.get("password");
        String displayName = input.get("displayName");
        Optional<User> existingUser = Optional.ofNullable(userRepository.findByUserName(userName));
        if (!existingUser.isPresent()) {
            User user = new User();
            user.setUserName(userName);
            user.setPassword(password);
            user.setDisplayName(displayName);
            UserTracker userTracker = new UserTracker();
            LocalDateTime myDateObj = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDate = myDateObj.format(myFormatObj);
            userTracker.setDate(formattedDate);
            user.setUserTracker(userTracker);
            userRepository.save(user);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("User has been added");
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body("User already exist");
    }

    @Override
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<User> users = userRepository.findAll();
        List<UserDTO> userList = new ArrayList<UserDTO>();
        users.forEach(x -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setUserName(x.getUserName());
            userDTO.setDisplayName(x.getDisplayName());
            userDTO.setDate(x.getUserTracker().getDate());
            if (!ObjectUtils.isEmpty(x.getGroup())) {
                userDTO.setGroupName(x.getGroup().getGroupName());
            }
            userList.add(userDTO);
        });
        return ResponseEntity.ok(userList);
    }

    @Override
    public ResponseEntity<ExpectedReturnDTO> getUser(HashMap<String, String> input) {
        String userName = input.get("userName");
        Optional<User> userDetails = Optional.ofNullable(userRepository.findByUserName(userName));
        ExpectedReturnDTO expectedReturnDto = new ExpectedReturnDTO();
        if (userDetails.isPresent()) {
            User user = userDetails.get();
            UserDTO userDTO = new UserDTO();
            userDTO.setUserName(user.getUserName());
            userDTO.setDisplayName(user.getDisplayName());
            userDTO.setDate(user.getUserTracker().getDate());
            if (!ObjectUtils.isEmpty(user.getGroup())) {
                userDTO.setGroupName(user.getGroup().getGroupName());
            }
            expectedReturnDto.setStatus(HttpStatus.OK);
            expectedReturnDto.setReturnObject(userDTO);
            return ResponseEntity.ok(expectedReturnDto);
        } else {
            expectedReturnDto.setReturnObject("User Not Found");
            expectedReturnDto.setStatus(HttpStatus.NOT_FOUND);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(expectedReturnDto);
        }
    }

    @Override
    public ResponseEntity<ExpectedReturnDTO> startCount(HashMap<String, String> input) {
        String userName = input.get("userName");
        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(userName));
        ExpectedReturnDTO expectedReturnDto = new ExpectedReturnDTO();
        if (user.isPresent()) {
            UserTracker userTracker = user.get().getUserTracker();
            LocalDateTime myDateObj = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedDate = myDateObj.format(myFormatObj);
            userTracker.setDate(formattedDate);
            userTrackerRepository.updateDate(formattedDate, userTracker.getUserTrackerId());
            expectedReturnDto.setStatus(HttpStatus.OK);
            expectedReturnDto.setReturnObject(formattedDate);
            return ResponseEntity.ok(expectedReturnDto);
        } else {
            expectedReturnDto.setStatus(HttpStatus.NOT_FOUND);
            expectedReturnDto.setReturnObject("User not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(expectedReturnDto);
        }

    }

    @Override
    public ResponseEntity<List<UserBoardDTO>> viewBoard(String groupName) {
        Optional<Group> groupDetails = Optional.ofNullable(groupRepository.findByGroupName(groupName));
        List<UserBoardDTO> userBoardDTOList = new ArrayList<UserBoardDTO>();
        if (groupDetails.isPresent()) {
            List<User> users = userRepository.viewBoardList(groupDetails.get().getGroupId());
            users.forEach(x -> {
                UserBoardDTO userBoardDTO = new UserBoardDTO();
                userBoardDTO.setUserName(x.getUserName());
                LocalDateTime myDateObj = LocalDateTime.now();
                DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                String formattedDate = myDateObj.format(myFormatObj);
                String latestDate = UserServiceImpl.dateFinder(x.getUserTracker().getDate(), formattedDate);
                userBoardDTO.setDate(latestDate);
                userBoardDTOList.add(userBoardDTO);
            });
            Collections.sort(userBoardDTOList, Collections.reverseOrder());
            return ResponseEntity.ok(userBoardDTOList);
        }
        return ResponseEntity.ok(userBoardDTOList);
    }

    @Override
    public ResponseEntity<String> removeFromGroup(HashMap<String, String> input) {
        String userName = input.get("userName");
        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(userName));
        if (user.isPresent()) {
            if (!ObjectUtils.isEmpty(user.get().getGroup())) {
                User userDetails = user.get();
                userDetails.setGroup(null);
                userDetails.setAdminFlag(null);
                userRepository.save(userDetails);
                return ResponseEntity.ok("Group Removed Successfully");
            } else {
                return ResponseEntity.ok("You are not in any group...");
            }
        } else {
            return ResponseEntity.ok("User Not Found");
        }

    }

    @Override
    public ResponseEntity<String> deleteUser(HashMap<String, String> input) {
        String userName = input.get("userName");
        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(userName));
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return ResponseEntity.ok("user has been Removed Successfully");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No user found");
    }

    @Override
    public ResponseEntity<ExpectedReturnDTO> login(HashMap<String, String> input) {

        String enteredUserName = input.get("userName");
        String enteredPassword = input.get("password");
        ExpectedReturnDTO expectedReturnDto = new ExpectedReturnDTO();

        Optional<User> user = Optional.ofNullable(userRepository.findByUserName(enteredUserName));
        if (user.isPresent()) {
            User userDetail = user.get();
            if (enteredUserName.equals(userDetail.getUserName())) {
                if (enteredPassword.equals(userDetail.getPassword())) {
                    UserDTO userDTO = new UserDTO();
                    userDTO.setUserName(userDetail.getUserName());
                    userDTO.setDisplayName(userDetail.getDisplayName());
                    if (!ObjectUtils.isEmpty(userDetail.getGroup())) {
                        userDTO.setGroupName(userDetail.getGroup().getGroupName());
                    }
                    String lastDateHistory = userDetail.getUserTracker().getDate();
                    LocalDateTime myDateObj = LocalDateTime.now();
                    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String newDate = myDateObj.format(myFormatObj);
                    String dateDifference = UserServiceImpl.dateFinder(lastDateHistory, newDate);
                    userDTO.setDate(dateDifference);
                    expectedReturnDto.setStatus(HttpStatus.OK);
                    expectedReturnDto.setReturnObject(user);

                    return new ResponseEntity<>(expectedReturnDto, HttpStatus.OK);
                } else {
                    expectedReturnDto.setStatus(HttpStatus.OK);
                    expectedReturnDto.setReturnObject("Password is Incorrect");
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(expectedReturnDto);
                }
            }
        }
        expectedReturnDto.setStatus(HttpStatus.NOT_FOUND);
        expectedReturnDto.setReturnObject("User Not Found");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(expectedReturnDto);
    }

}
