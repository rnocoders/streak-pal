package com.streakapp.streakpal.service;

import com.streakapp.streakpal.dto.ExpectedReturnDTO;
import com.streakapp.streakpal.dto.UserBoardDTO;
import com.streakapp.streakpal.dto.UserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface UserService {

    public ResponseEntity<String> saveUser(HashMap<String,String> input);
    public ResponseEntity<List<UserDTO>> getUsers();
    public ResponseEntity<ExpectedReturnDTO> getUser(HashMap<String,String> input);
    public ResponseEntity<ExpectedReturnDTO> startCount(HashMap<String,String> input);

    public ResponseEntity<List<UserBoardDTO>> viewBoard(String groupName);

    public ResponseEntity<String> removeFromGroup(HashMap<String,String> input);

    public ResponseEntity<String> deleteUser(HashMap<String,String> input);

    public ResponseEntity<ExpectedReturnDTO> login(HashMap<String,String> input);

}
