package com.streakapp.streakpal.service;

import com.streakapp.streakpal.entity.Group;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;

@Service
public interface GroupService {

    public ResponseEntity<String> createGroup(@RequestBody HashMap<String,String> input);

    public ResponseEntity<String> addGroupMembers(HashMap<String,String> input);

    public ResponseEntity<String> deleteGroup(HashMap<String,String> input);

    boolean isInGroup(String userName);

    List<Group> listOfGroups();
}
