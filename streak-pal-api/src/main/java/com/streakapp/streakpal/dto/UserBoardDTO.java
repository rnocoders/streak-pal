package com.streakapp.streakpal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBoardDTO implements Comparable {

    private String userName;
    private String date;

    @Override
    public int compareTo(Object o) {
        return this.getDate().compareTo(((UserBoardDTO) o).getDate());
    }
}
