package com.streakapp.streakpal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpectedReturnDTO {

    private HttpStatus status;
    private Object returnObject;
}