package com.streakapp.streakpal.controller;

import com.streakapp.streakpal.dto.ExpectedReturnDTO;
import com.streakapp.streakpal.dto.UserBoardDTO;
import com.streakapp.streakpal.dto.UserDTO;
import com.streakapp.streakpal.entity.Group;
import com.streakapp.streakpal.service.GroupService;
import com.streakapp.streakpal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin
public class StreakController {

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @PostMapping("/saveUserDetails")
    public ResponseEntity<String> saveUserDetails(@RequestBody HashMap<String, String> input) {

        return userService.saveUser(input);
    }

    @GetMapping("/getUsers")
    public ResponseEntity<List<UserDTO>> getUsers(@RequestBody HashMap<String, String> input) {

        return userService.getUsers();
    }

    @GetMapping("/getUser")
    public ResponseEntity<ExpectedReturnDTO> getUser(@RequestBody HashMap<String, String> input) {

        return userService.getUser(input);
    }

    @PutMapping("/startCount")
    public ResponseEntity<ExpectedReturnDTO> startCount(@RequestBody HashMap<String, String> input) {

        return userService.startCount(input);
    }

    @PostMapping("/createGroup")
    public ResponseEntity<String> createGroup(@RequestBody HashMap<String, String> input) {

        return groupService.createGroup(input);
    }

    @PutMapping("/joinGroup")
    public ResponseEntity<String> addGroupMembers(@RequestBody HashMap<String, String> input) {

        return groupService.addGroupMembers(input);
    }

    @GetMapping("/viewBoard")
    public ResponseEntity<List<UserBoardDTO>> viewLeaderBoard(@RequestParam("groupname") String groupName) {

        return userService.viewBoard(groupName);
    }

    @PutMapping("/removeFromGroup")
    public ResponseEntity<String> removeFromGroup(@RequestBody HashMap<String, String> input) {

        return userService.removeFromGroup(input);
    }

    @PostMapping("/deleteGroup")
    public ResponseEntity<String> deleteGroup(@RequestBody HashMap<String, String> input) {

        return groupService.deleteGroup(input);
    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity<String> deleteUser(@RequestBody HashMap<String, String> input) {

        return userService.deleteUser(input);
    }

    @PostMapping("/login")
    public ResponseEntity<ExpectedReturnDTO> login(@RequestBody HashMap<String, String> input) {

        return userService.login(input);
    }

    @GetMapping("/isInGroup")
    public boolean isInGroup(@RequestParam("userName") String userName) {
        return groupService.isInGroup(userName);
    }

    @GetMapping("/listOfGroups")
    public List<Group> listOfGroups() {
        return groupService.listOfGroups();
    }


}
